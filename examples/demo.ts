import { app } from '../src/'

app.configurateDatabase({
    driverName: 'postgres',
    connectionOption: {
        connectionString: process.env.DATABASE_URL || '',
    },
})
    .runMigration()
    .then(() => {
        console.log('Succesfuly run database migration')
        app.runApplication()
    })
    .catch((e) => {
        console.log(`Fail to start the server ${e}`)
        throw e
    })

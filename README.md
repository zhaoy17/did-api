# DID-Server

[![CircleCI](https://circleci.com/gh/zhaoy17/did-server/tree/main.svg?style=shield)](https://circleci.com/gh/zhaoy17/did-server/tree/main)

DID (Data Interface Database) allows you to set up your own server-side web API easily, which can be used to store, query and share scientific data of any type. Clients can interact with the API through HTTP protocol. As almost all programming languages offer either built-in or open source http client libraries, it is possible to integrate the underlying data analysis code written in any language with the service.

## Prerequisite
The server can run both locally and in the cloud. In either case, you must provide connection configuration to a database and a file server. DID aims to support a wide variety of databases including the ones listed below natively. In cases where the database you are using is not natively supported, you can easily implements custom Repository classes that return the data from the database you are using.

### Databases supported

| name          | status                          |
|---------------|---------------------------------|
| Postgres      | supported, but not tested       |
| MySql         | will be supported in the future |
| Oracle        | will be supported in the future |
| MS SQL Server | will be supported in the future |
| SqlLite       | will be supported in the future |
| MongoDB       | will be supported in the future |

### File Server Supported
Will be added in the future

## Type of files you can store

### DIDDocument:
A JSON like file, which may contain any property suitable for one's needs. Furthermore, a DIDDocument can derive properties from another DIDDocument (analogous to subclassing in Object-oriented programming). A DIDDocument can have properties with values that are primitive types (string, number, boolean) or complex type (list, map). In addition, the properties may have values that point to another DIDDocument or BinaryDocument, thus forming a tree-like hierarchical structure. DIDDocuments are stored in the database provided by the user.

### BinaryDocument:
Files that contain sequences of bytes. It can be formatted text, images or videos. They are stored in the file server provided by the user.

## Getting started

### Installation
You can install DID-API globally:

```
npm install did-api -g
```

### Configurate Database

```typescript
import { app } from "did-api"

app.configurateDatabase(
{
  driverName: "postgres",
  connectionOption:
  {
    user: "postgres",
    password: "",
    host: "localhost",
    port: 5432
  }
})
```

### Configurate File Server
To be added

### Configurate Validation Service
To be added

### Configurate Document Service
To be added

### Configurate Version Control Service
To be added

### Running Database Migration

```typescript
app.runMigration()
```

### Running the application
To be added

## API
API documentation is published on [Netlify](https://did-server.netlify.app/) based on the [OpenAPI Specification](https://swagger.io/specification/) document, which can be found [here](/docs/api-spec/api-v1.yml).


## Database Design
The latest version of the database schema is version **0**.

The database schema is documented using [DBML](https://www.dbml.org/home/) (Database Markup Lanauge), which can be found [here](/docs/dbml/v0.dbml). We also host the database documentation on [dbdocs](https://dbdocs.io/peterzhao204/DID), which offers nice visualization of the tables in the database.

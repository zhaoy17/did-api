CREATE TABLE "migrations" (
  "time_created" timestamp DEFAULT (CURRENT_TIMESTAMP),
  "did_version" NUMERIC NOT NULL,
  "dbschema_version" NUMERIC NOT NULL,
  PRIMARY KEY ("did_version", "dbschema_version")
);

CREATE TABLE "database" (
  "database_id" int4 PRIMARY KEY NOT NULL,
  "database_name" varchar UNIQUE NOT NULL,
  "description" varchar NOT NULL
);

CREATE TABLE "snapshot" (
  "snapshot_id" varchar NOT NULL,
  "version" varchar NOT NULL,
  "id" varchar NOT NULL,
  "database_id" int4 NOT NULL,
  PRIMARY KEY ("version", "id", "database_id")
);

CREATE TABLE "snapshot_hash" (
  "snapshot_id" varchar PRIMARY KEY NOT NULL,
  "version" varchar NOT NULL
);

CREATE TABLE "commit" (
  "commit_id" varchar PRIMARY KEY NOT NULL,
  "last_commit_id" varchar NOT NULL,
  "snapshot_id" varchar,
  "message" varchar,
  "time_created" timestamp NOT NULL,
  "user_id" varchar NOT NULL
);

CREATE TABLE "branch" (
  "branch_id" varchar NOT NULL,
  "commit_id" varchar NOT NULL,
  "current_working_snapshot_id" varchar NOT NULL,
  "database_id" int4 NOT NULL,
  PRIMARY KEY ("branch_id", "database_id")
);

CREATE TABLE "pullrequest" (
  "request_id" int4 PRIMARY KEY NOT NULL,
  "source_branch_id" varchar NOT NULL
);

CREATE TABLE "comments" (
  "comment_id" int4 PRIMARY KEY NOT NULL,
  "request_id" int4 NOT NULL,
  "topic" varchar NOT NULL,
  "message" varchar NOT NULL
);

CREATE TABLE "schema" (
  "schema_id" varchar NOT NULL,
  "schema_name" varchar NOT NULL,
  "schema" jsonb NOT NULL,
  "schema_version" varchar NOT NULL,
  PRIMARY KEY ("schema_id", "schema_version")
);

CREATE TABLE "schemainherentance" (
  "schema_id" varchar NOT NULL,
  "schema_version" varchar NOT NULL,
  "extended_by_id" varchar,
  "extended_by_version" varchar
);

CREATE TABLE "document" (
  "document_id" varchar NOT NULL,
  "document_version" varchar NOT NULL,
  "schema_id" varchar NOT NULL,
  "schema_version" varchar NOT NULL,
  "document" jsonb NOT NULL,
  "time_created" timestamp NOT NULL,
  PRIMARY KEY ("document_id", "document_version")
);

CREATE TABLE "documentsdependency" (
  "document_id" varchar NOT NULL,
  "document_version" varchar NOT NULL,
  "depended_by_document_version" varchar,
  "depended_by_document_id" varchar,
  PRIMARY KEY ("document_version", "document_id")
);

ALTER TABLE "snapshot" ADD FOREIGN KEY ("database_id") REFERENCES "database" ("database_id") ON DELETE RESTRICT;

ALTER TABLE "snapshot" ADD FOREIGN KEY ("snapshot_id") REFERENCES "snapshot_hash" ("snapshot_id");

ALTER TABLE "commit" ADD FOREIGN KEY ("last_commit_id") REFERENCES "commit" ("commit_id") ON DELETE RESTRICT;

ALTER TABLE "branch" ADD FOREIGN KEY ("commit_id") REFERENCES "commit" ("commit_id") ON DELETE RESTRICT;

ALTER TABLE "pullrequest" ADD FOREIGN KEY ("source_branch_id") REFERENCES "branch" ("branch_id");

ALTER TABLE "branch" ADD FOREIGN KEY ("database_id") REFERENCES "database" ("database_id") ON DELETE RESTRICT;

ALTER TABLE "comments" ADD FOREIGN KEY ("request_id") REFERENCES "pullrequest" ("request_id");

ALTER TABLE "schemainherentance" ADD FOREIGN KEY ("schema_id", "schema_version") REFERENCES "schema" ("schema_id", "schema_version") ON DELETE CASCADE;

ALTER TABLE "schemainherentance" ADD FOREIGN KEY ("extended_by_id", "extended_by_version") REFERENCES "schema" ("schema_id", "schema_version") ON DELETE RESTRICT;

ALTER TABLE "document" ADD FOREIGN KEY ("schema_id", "schema_version") REFERENCES "schema" ("schema_id", "schema_version") ON DELETE RESTRICT;

ALTER TABLE "documentsdependency" ADD FOREIGN KEY ("document_version", "document_id") REFERENCES "document" ("document_version", "document_id");

ALTER TABLE "documentsdependency" ADD FOREIGN KEY ("depended_by_document_version", "depended_by_document_id") REFERENCES "document" ("document_version", "document_id") ON DELETE RESTRICT;

CREATE INDEX ON "snapshot" USING BTREE ("snapshot_id", "id", "database_id");

CREATE INDEX ON "schema" USING BTREE ("schema_id", "schema_version");

CREATE UNIQUE INDEX ON "document" USING BTREE ("document_version", "document_id");

CREATE INDEX ON "document" USING BTREE ("document_version", "document");

CREATE INDEX ON "documentsdependency" USING BTREE ("document_version", "document_id");

CREATE INDEX ON "documentsdependency" USING BTREE ("depended_by_document_version", "depended_by_document_id");

COMMENT ON TABLE "migrations" IS 'Records in this table are used by MigrationRunner to keep track the version of the database schema. For example, if the
the records with the latest time_created has dbschema_version = 0, and the latest dbschema_version = 2, migration runner will
first upgrade the database schema to version 1, and then to upgrade it to version 2.';

COMMENT ON COLUMN "migrations"."time_created" IS 'the time & date when the database migration is run';

COMMENT ON COLUMN "migrations"."did_version" IS 'the version of the did server';

COMMENT ON COLUMN "migrations"."dbschema_version" IS 'the version of the database schema';

COMMENT ON TABLE "database" IS 'Databases may be used to organize the data stroed by the server into groups. Each database contains
a collection of DIDDocuments and BinaryFiles.';

COMMENT ON COLUMN "database"."database_id" IS 'the unique identification of the services';

COMMENT ON COLUMN "database"."database_name" IS 'the name of the database';

COMMENT ON COLUMN "database"."description" IS 'description of the service';

COMMENT ON TABLE "snapshot" IS 'A snapshot represents the database at a particular state. It holds a collection of files/documents.
A snapshot of a given database cannont contain multiple versions of the same file/document. As a consequence,
a file"s/document"s version id and database_id should uniquely determine a snapshot. This is why they serve as
the primary keys of this table.';

COMMENT ON COLUMN "snapshot"."snapshot_id" IS 'the unique identification of a snapshot record';

COMMENT ON COLUMN "snapshot"."version" IS 'the hash version number of the file/document it holds (two files/documents that have the same content
should have the same hash version number)';

COMMENT ON COLUMN "snapshot"."id" IS 'the unique identification of the file/document that the snapshot holds';

COMMENT ON COLUMN "snapshot"."database_id" IS 'the database that the snapshot belongs to';

COMMENT ON TABLE "snapshot_hash" IS 'This table holds the hash values of the snapshots. By comparing the hash value of the current
working snapshot and the one of which pointed by the last commit, we can easily determine
if any modification has been made to the original snapshot.';

COMMENT ON COLUMN "snapshot_hash"."snapshot_id" IS 'The unique identification of a snapshot';

COMMENT ON COLUMN "snapshot_hash"."version" IS 'The hash version of the snapshot. Two snapshots that hold the files/documents with the same content
should have the same hash value.';

COMMENT ON TABLE "commit" IS 'A commit describes a snapshot of the database"s staged changes. Each commit has a commit message, which
describe the changes that have been made since the last commit has been created. A snapshot may be pointed
by multiple commits, but each commit must point to a particular snapshot.';

COMMENT ON COLUMN "commit"."commit_id" IS 'The unique identification of a commit.';

COMMENT ON COLUMN "commit"."last_commit_id" IS 'The unique identification of the prior commit.';

COMMENT ON COLUMN "commit"."snapshot_id" IS 'The unique identification of the snapshot the commit points to';

COMMENT ON COLUMN "commit"."message" IS 'The commit message.';

COMMENT ON COLUMN "commit"."time_created" IS 'The time & date when the commit was created';

COMMENT ON COLUMN "commit"."user_id" IS 'The unique identification of the user who created this commit';

COMMENT ON TABLE "branch" IS 'This table holds all the branches opened in each database. As multiple people may be working on the same project, each person may
open a new branch, which usually initially points to the commit that the main branch points to. Then the person may make changes
to his or her own branch, and creates new commits on top of the commit the branch originally points to. Eventually the user may
merge his or her own branch into the main branch, and the new commits that have been made will be appended to the ones in the main branch.';

COMMENT ON COLUMN "branch"."branch_id" IS 'The unique identification of the branch';

COMMENT ON COLUMN "branch"."commit_id" IS 'The commit that the branch points to';

COMMENT ON COLUMN "branch"."current_working_snapshot_id" IS 'The snapshot that contains uncommited changes for this branch';

COMMENT ON COLUMN "branch"."database_id" IS 'The database this branch belongs to';

COMMENT ON TABLE "pullrequest" IS 'Before a branch can be merged into the main branch, a pull request needs to be made and be approved. This table holds all the
pull requests that have been made.';

COMMENT ON COLUMN "pullrequest"."request_id" IS 'The unique identification of a pull request';

COMMENT ON COLUMN "pullrequest"."source_branch_id" IS 'The unique idenfitication of the branch requested to be merged into the main branch';

COMMENT ON TABLE "comments" IS 'This table holds all the comments that a reviewer has made in regards to a pull request.';

COMMENT ON COLUMN "comments"."comment_id" IS 'The unique identification of the comment.';

COMMENT ON COLUMN "comments"."request_id" IS 'The unique identification of the pull request the comment is about.';

COMMENT ON COLUMN "comments"."topic" IS 'What this comment is about.';

COMMENT ON COLUMN "comments"."message" IS 'The actual comment.';

COMMENT ON TABLE "schema" IS 'This table holds all the schemas specified for the DIDDocuments. A schema specifies the fields a
DIDDocument is required to hold. When a DIDDocument is modified or inserted into the database, the
document will be validated against the schema. A schema can also extend from another schema. A schema
is analogous to an interface in Object-Oriented Programming.';

COMMENT ON COLUMN "schema"."schema_id" IS 'The unique identification of the schema';

COMMENT ON COLUMN "schema"."schema_name" IS 'The name of the schema';

COMMENT ON COLUMN "schema"."schema" IS 'The content of the schema';

COMMENT ON COLUMN "schema"."schema_version" IS 'The hash version number of the schema';

COMMENT ON TABLE "schemainherentance" IS 'This table keeps track of all the inherentance relationships exist among the all the schemas.';

COMMENT ON COLUMN "schemainherentance"."schema_id" IS 'The unique identification of a schema.';

COMMENT ON COLUMN "schemainherentance"."schema_version" IS 'The hash version number of the schema.';

COMMENT ON COLUMN "schemainherentance"."extended_by_id" IS 'The unique identification of the schema extended by this schema.';

COMMENT ON COLUMN "schemainherentance"."extended_by_version" IS 'The hash version number of the schema extended by this schema.';

COMMENT ON TABLE "document" IS 'This table contains all the DIDDocuments (a JSON-like file with key-value pairs) that have been created in
the database. Note that it is possible for a DIDDocument to has a field with value that points to another
DIDDocument. A document is analogous to an object in Object-Oriented Programming.';

COMMENT ON COLUMN "document"."document_id" IS 'The unique identification of the document.';

COMMENT ON COLUMN "document"."document_version" IS 'The hash version number of the document.';

COMMENT ON COLUMN "document"."schema_id" IS 'The unique identification of the schema this document belongs to.';

COMMENT ON COLUMN "document"."schema_version" IS 'The hash version of the schema this document belongs to.';

COMMENT ON COLUMN "document"."document" IS 'The content of the document.';

COMMENT ON TABLE "documentsdependency" IS 'This table contains all the dependency relationships exist among the DIDDocuments. When a DIDDocument contains a field that points
to another DIDDocument, we call the latter to be depended by the former.';

COMMENT ON COLUMN "documentsdependency"."document_id" IS 'The unique identification of the document.';

COMMENT ON COLUMN "documentsdependency"."document_version" IS 'The hash version number of the document.';

COMMENT ON COLUMN "documentsdependency"."depended_by_document_version" IS 'The hash version number of the document depended by this document.';

COMMENT ON COLUMN "documentsdependency"."depended_by_document_id" IS 'The unique identificatioin of the document depended by this document.';

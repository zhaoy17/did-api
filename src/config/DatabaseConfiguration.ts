import { PostgresConnection } from '../database/drivers/PotgresDriver'

export type DriverName = 'postgres'

export type ConnectionOption = PostgresConnection

export type DatabaseConfiguration = {
    driverName: DriverName
    connectionOption: ConnectionOption
}

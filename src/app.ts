import MigrationRunner from './migrations/MigrationRunner'
import { Driver } from './database/drivers/Driver'
import { DatabaseConfiguration } from './config/DatabaseConfiguration'
import { PostgresDriver } from './database/drivers/PotgresDriver'
import express from 'express'

export class ApplicationNotConfiguredError extends Error {
    constructor(message?: string) {
        super(message)
        Object.setPrototypeOf(this, ApplicationNotConfiguredError.prototype)
    }
}

export class ConfigurationNotSupportedError extends Error {
    constructor(message?: string) {
        super(message)
        Object.setPrototypeOf(this, ConfigurationNotSupportedError.prototype)
    }
}

class Application {
    driver: Driver | null = null
    migration: MigrationRunner | null = null
    app = express()
    port = process.env.PORT || 3000

    configurateDatabase(databaseConfig: DatabaseConfiguration) {
        if (databaseConfig.driverName == 'postgres') {
            this.driver = new PostgresDriver(databaseConfig.connectionOption)
        } else {
            throw new ConfigurationNotSupportedError(
                `${databaseConfig.driverName} is not supported`
            )
        }
        return this
    }

    async runMigration() {
        if (this.migration == null) {
            if (this.driver == null) {
                throw new ApplicationNotConfiguredError(
                    'Database not configured; configureDatabase() needs to be called'
                )
            }
            this.migration = new MigrationRunner(this.driver)
        }
        await this.migration.applyMigration()
    }

    runApplication() {
        this.app.listen(this.port, () => {
            console.log(`DID server is currently running on port ${this.port}`)
        })
    }
}

export const app = new Application()

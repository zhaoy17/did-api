import ieee754 from 'ieee754'

const MAX_RAND = 32728
const MIN_RAND = -32728

const OFFSET = 0
const IS_LE = false
const MANTISSA_LENGTH = 52
const NUMBER_OF_BYTES = 8

export class InvalidIdError extends Error {
    constructor(message?: string) {
        super(message)
        Object.setPrototypeOf(this, InvalidIdError.prototype)
    }
}

export default class Id {
    timestamp: number
    randInt: number

    constructor(id?: string) {
        if (id === undefined) {
            this.timestamp = new Date().getTime() / 1000
            this.randInt = Math.random() * (MAX_RAND - MIN_RAND + MIN_RAND)
        } else {
            try {
                const splitId: string[] = id.split('_', 2)
                this.timestamp = this.fromHex(splitId[0])
                this.randInt = this.fromHex(splitId[1])
            } catch (e) {
                throw new InvalidIdError('Fail to parse id. It may not be valid')
            }
            if (Number.isNaN(this.timestamp) || Number.isNaN(this.randInt)) {
                throw new InvalidIdError('Fail to parse id. It may not be valid')
            }
        }
    }

    toString(): string {
        return `${this.toHex(this.timestamp)}_${this.toHex(this.randInt)}`
    }

    compareTo(id: Id): number {
        return this.timestamp - id.timestamp
    }

    private toHex(num: number): string {
        const hex = []
        const buffer = Buffer.alloc(NUMBER_OF_BYTES)
        ieee754.write(buffer, num, OFFSET, IS_LE, MANTISSA_LENGTH, NUMBER_OF_BYTES)
        for (const bit of new Uint8Array(buffer)) {
            hex.push(('00' + bit.toString(16)).slice(-2))
        }
        return hex.join('')
    }

    private fromHex(hex: string): number {
        return ieee754.read(
            Buffer.from(hex, 'hex'),
            OFFSET,
            IS_LE,
            MANTISSA_LENGTH,
            NUMBER_OF_BYTES
        )
    }
}

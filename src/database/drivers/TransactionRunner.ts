/* eslint-disable @typescript-eslint/no-explicit-any */

export interface TransactionRunner {
    driverName: string

    query<T>(rawQuery: string, params: any[], model: new () => T): Promise<T[]>

    execute(rawQuery: string, params: any[]): Promise<any>

    commit(): Promise<void>

    rollback(): Promise<void>
}

/* eslint-disable @typescript-eslint/no-explicit-any */

import { TransactionRunner } from './TransactionRunner'

export interface Driver {
    driverName: string

    startTransaction(): Promise<TransactionRunner>

    execute(rawQuery: string, params: any[]): Promise<any>

    query<T>(rawQuery: string, params: any[], model: new (...args: any[]) => T): Promise<T[]>
}

export class DriverError extends Error {
    constructor(message?: string) {
        super(message)
        Object.setPrototypeOf(this, DriverError.prototype)
    }
}

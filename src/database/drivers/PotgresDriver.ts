/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
/* eslint-disable @typescript-eslint/no-explicit-any */

import { Driver, DriverError } from './Driver'
import { Pool, PoolClient } from 'pg'
import { MapperError, mapFromTo } from '../../mapper'
import { TransactionRunner } from './TransactionRunner'

type PostgresConnectionOptions = {
    user: string
    host: string
    password: string
    database: string
    port: number
    maxPool?: number
}

type PostgresConnectionString = {
    connectionString: string
    maxPool?: number
}

export type PostgresConnection = PostgresConnectionOptions | PostgresConnectionString

export class PostgresDriver implements Driver {
    pool: Pool
    driverName: string
    client: PoolClient | undefined

    constructor(config?: any, pool?: Pool) {
        this.driverName = 'postgres'
        if (config != null) {
            this.pool = this.getConnectionPool(config)
        } else if (pool != null) {
            this.pool = pool
        } else {
            throw new DriverError(
                'either an instance of PostgresConnection or an instance of Pool needs to be passed in'
            )
        }
    }

    getConnectionPool(config: any): Pool {
        const connectionString = config['connectionString']
        if (connectionString == null) {
            const user = config['user']
            const host = config['host']
            const database = config['database']
            const password = config['password']
            const port = config['port']
            const maxPool = config['maxPool']

            if (user == null || typeof user != 'string') {
                throw new DriverError('user is not optional and must be a string')
            }
            if (host == null || typeof user != 'string') {
                throw new DriverError('host is not optional and must be a string')
            }
            if (database == null || typeof user != 'string') {
                throw new DriverError('database is not optional and must be a string')
            }
            if (password == null || typeof password != 'string') {
                throw new DriverError('password is not optional and must be a string')
            }
            if (port == null || typeof port != 'number') {
                throw new DriverError('port is not optional and must be a number')
            }
            return new Pool({
                user: user,
                host: host,
                database: database,
                password: password,
                port: port,
                connectionTimeoutMillis: 2000,
                max: maxPool || 20,
            })
        }
        return new Pool({
            connectionString: connectionString,
            ssl: {
                rejectUnauthorized: false,
            },
        })
    }

    async startTransaction(): Promise<TransactionRunner> {
        const client = await this.pool.connect()
        if (client == null) {
            throw new Error('Fail to start transaction')
        }
        try {
            await client.query('BEGIN')
        } catch (e) {
            throw new DriverError(`Fail to start transaction: ${e}`)
        }
        return new PostgresTransactionRunner(client)
    }

    async query<T>(
        rawQuery: string,
        params: any[],
        model: new (...args: any[]) => T
    ): Promise<T[]> {
        return queryWithClient(rawQuery, params, this.pool, model)
    }

    async execute(rawQuery: string, params: any[]): Promise<any> {
        const client = this.client == undefined ? this.pool : this.client
        try {
            return (await this.pool.query(rawQuery, params)).rows
        } catch (err) {
            throw new DriverError(`driver error: ${err}`)
        } finally {
            if (client == this.client) {
                client.release()
            }
        }
    }
}

export class PostgresTransactionRunner implements TransactionRunner {
    driverName: string
    client: PoolClient
    private hasReleased: boolean

    constructor(client: PoolClient) {
        this.driverName = 'postgres'
        this.client = client
        this.hasReleased = false
    }

    async query<T>(
        rawQuery: string,
        params: any[],
        model: new (...args: any[]) => T
    ): Promise<T[]> {
        if (this.hasReleased) {
            throw new DriverError('Transaction has been closed')
        }
        return queryWithClient(rawQuery, params, this.client, model)
    }

    async commit(): Promise<void> {
        if (this.hasReleased) {
            throw new DriverError('transaction has been closed')
        }
        try {
            await this.client.query('COMMIT')
        } catch (err) {
            throw new DriverError(`driver error ${err}`)
        } finally {
            this.client.release()
            this.hasReleased = true
        }
    }

    async rollback(): Promise<void> {
        if (this.hasReleased) {
            throw new DriverError('transaction has been closed')
        }
        try {
            await this.client.query('ROLLBACK')
        } catch (err) {
            throw new DriverError(`driver error ${err}`)
        } finally {
            this.client.release()
            this.hasReleased = true
        }
    }

    async execute(rawQuery: string, params: any[]): Promise<any> {
        if (this.hasReleased) {
            throw new DriverError('Transaction has been closed')
        }
        try {
            return (await this.client.query(rawQuery, params)).rows
        } catch (err) {
            console.log(rawQuery)
            throw new DriverError(`driver error: ${err}`)
        }
    }
}

async function queryWithClient<T>(
    rawQuery: string,
    params: any[],
    client: PoolClient | Pool,
    model: new () => T
): Promise<T[]> {
    try {
        const queries = (await client.query(rawQuery, params)).rows
        if (Array.isArray(queries)) {
            return queries.map((query) => mapFromTo(query, model))
        } else {
            throw new DriverError('driver returns unexpected output')
        }
    } catch (err) {
        if (err instanceof MapperError) {
            throw err
        }
        throw new DriverError(`driver error: ${err}`)
    }
}

class TreeNode<T> {
    id: string
    data: T
    children: string[]

    constructor(data: T, id: string) {
        this.data = data
        this.id = id
        this.children = []
    }
}

export class InvalidTreeOperationError extends Error {
    constructor(message?: string) {
        super(message)
        Object.setPrototypeOf(this, InvalidTreeOperationError.prototype)
    }
}

export class Tree<T> {
    private root: TreeNode<T>
    private nodeMap: Map<string, TreeNode<T>>

    constructor(data: T, rootNodeId: string) {
        this.root = new TreeNode(data, rootNodeId)
        this.nodeMap = new Map()
        this.nodeMap.set(rootNodeId, this.root)
    }

    getNodeData(nodeId: string): T {
        const node = this.nodeMap.get(nodeId)
        if (node !== undefined) {
            return node.data
        } else {
            throw new InvalidTreeOperationError(`Node with an id ${nodeId} does not exist`)
        }
    }

    getRootId(): string {
        return this.root.id
    }

    setNodeData(nodeId: string, value: T): void {
        const node = this.nodeMap.get(nodeId)
        if (node !== undefined) {
            node.data = value
        } else {
            throw new InvalidTreeOperationError(`node with id ${nodeId} does not exist`)
        }
    }

    addChild(parentNodeId: string, childNodeId: string, childNodeData: T): void {
        const parent = this.nodeMap.get(parentNodeId)
        if (parent !== undefined) {
            const childNode = new TreeNode(childNodeData, childNodeId)
            parent.children.push(childNodeId)
            this.nodeMap.set(childNodeId, childNode)
        } else {
            throw new InvalidTreeOperationError('parent node does not exist')
        }
    }

    getChildrenId(parentNodeId: string): string[] {
        const parentNode = this.nodeMap.get(parentNodeId)
        if (parentNode === undefined) {
            throw new InvalidTreeOperationError('Parent node does not exist')
        }
        return parentNode.children
    }

    isChild(parentNodeid: string, childNodeId: string): boolean {
        const parentNode = this.nodeMap.get(parentNodeid)
        const childNode = this.nodeMap.get(childNodeId)
        if (parentNode === undefined) {
            throw new InvalidTreeOperationError('Parent node does not exist')
        }
        if (childNode === undefined) {
            throw new InvalidTreeOperationError('Child node does not exist')
        }
        return parentNode.children.findIndex((data) => data == childNodeId) != -1
    }

    *preOrderTraversal(nodeId: string): Generator<T> {
        const node = this.nodeMap.get(nodeId)
        if (node !== undefined) {
            yield node.data
            for (const child of node.children) {
                for (const nodeData of this.preOrderTraversal(child)) {
                    yield nodeData
                }
            }
        } else {
            throw new InvalidTreeOperationError(`node with id ${nodeId} does not exist`)
        }
    }

    *postOrderTraversal(nodeId: string): Generator<T> {
        const node = this.nodeMap.get(nodeId)
        if (node !== undefined) {
            for (const child of node.children) {
                for (const nodeData of this.postOrderTraversal(child)) {
                    yield nodeData
                }
            }
            yield node.data
        } else {
            throw new InvalidTreeOperationError(`node with id ${nodeId} does not exist`)
        }
    }
}

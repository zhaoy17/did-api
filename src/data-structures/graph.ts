class Vertex<T> {
    id: string
    data: T
    edges: Set<string>

    constructor(id: string, data: T) {
        this.id = id
        this.data = data
        this.edges = new Set<string>()
    }
}

export class InvalidGraphOperationError extends Error {
    constructor(message?: string) {
        super(message)
        Object.setPrototypeOf(this, InvalidGraphOperationError.prototype)
    }
}

export class Graph<T> {
    adjList: Map<string, Vertex<T>>

    constructor() {
        this.adjList = new Map<string, Vertex<T>>()
    }

    getVertex(vertexId: string): T {
        const vertex = this.adjList.get(vertexId)
        if (vertex === undefined) {
            throw new InvalidGraphOperationError(
                `Vertex with an identifier ${vertexId} already exists`
            )
        } else {
            return vertex.data
        }
    }

    addVertex(vertexId: string, value: T): void {
        if (!this.adjList.has(vertexId)) {
            this.adjList.set(vertexId, new Vertex(vertexId, value))
        } else {
            throw new InvalidGraphOperationError(
                `Vertex with an identifier ${vertexId} already exists`
            )
        }
    }

    isConnected(fromVertexId: string, toVertexId: string): boolean {
        const vertex = this.adjList.get(fromVertexId)
        if (vertex === undefined) {
            throw new InvalidGraphOperationError(
                `Vertex with an identifier ${fromVertexId} does not exist exists`
            )
        }
        return vertex.edges.has(toVertexId)
    }

    deleteVertex(vertexId: string): void {
        const vertex = this.adjList.get(vertexId)
        if (vertex == undefined) {
            throw new InvalidGraphOperationError(
                `Vertex with an identifier ${vertexId} does not exist`
            )
        }
        for (const item of this.adjList.values()) {
            if (item.edges.has(vertexId)) {
                item.edges.delete(vertexId)
            }
        }
        this.adjList.delete(vertexId)
    }

    addEdge(firstVertex: string, secondVertex: string): void {
        if (!this.adjList.has(firstVertex) || !this.adjList.has(secondVertex)) {
            throw new InvalidGraphOperationError(
                `Vertex ${firstVertex} or ${secondVertex} does not exists`
            )
        }
        this.adjList.get(firstVertex)?.edges.add(secondVertex)
    }

    deleteEdge(firstVertex: string, secondVertex: string): void {
        if (!this.adjList.has(firstVertex) || !this.adjList.has(secondVertex)) {
            throw new InvalidGraphOperationError(
                `Vertex ${firstVertex} or ${secondVertex} does not exists`
            )
        }
        const edges = this.adjList.get(firstVertex)?.edges
        if (!edges?.has(secondVertex)) {
            throw new InvalidGraphOperationError(
                `Vertex ${firstVertex} and ${secondVertex} are not connected`
            )
        }
        edges.delete(secondVertex)
    }

    *dfs(start: string): Generator<T> {
        for (const item of this._dfsHelper(start, new Map<string, boolean>())) {
            yield item
        }
    }

    private *_dfsHelper(start: string, hasVisited: Map<string, boolean>): Generator<T> {
        if (!hasVisited.has(start)) {
            const vertex = this.adjList.get(start)
            if (vertex !== undefined) {
                hasVisited.set(start, true)
                yield vertex.data
            } else {
                throw new InvalidGraphOperationError(`Vertex ${start} does not exists`)
            }
            for (const vertexConnectedTo of vertex.edges.values()) {
                for (const vertexData of this._dfsHelper(vertexConnectedTo, hasVisited)) {
                    yield vertexData
                }
            }
        }
    }
}

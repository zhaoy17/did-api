/* eslint-disable @typescript-eslint/ban-types */
/* eslint-disable @typescript-eslint/no-explicit-any */
/* eslint-disable @typescript-eslint/explicit-module-boundary-types */

import 'reflect-metadata'

const primitiveType: Set<string> = new Set([String.name, Number.name, Boolean.name, Object.name])

type Constructor<T> = (new (...args: any[]) => T) | ((...args: any[]) => T)

type MappingRules<T> = {
    mapFromProperty: string
    type: Constructor<T>
    isRequired: boolean
    validator?: (value: any) => void
}

export class MapperError extends Error {
    constructor(message?: string) {
        super(message)
        Object.setPrototypeOf(this, MapperError.prototype)
    }
}

export function map(
    mapFromProperty: string,
    expectedType?: any,
    isRequired?: boolean,
    validator?: (value: any) => void
) {
    return (target: Object, propertyKey: string): void => {
        if (isRequired == null) {
            isRequired = true
        }
        if (expectedType == null) {
            if (!isRequired) {
                throw new MapperError('expectedType must be specified when isRequired is false')
            }
            expectedType = Reflect.getMetadata('design:type', target, propertyKey)
        }
        registerProperty(target, propertyKey, mapFromProperty, expectedType, isRequired, validator)
    }
}

export function mapFromTo<T>(from: any, target: Constructor<T>): T {
    const obj = Object.create(target.prototype)
    const properties = getPropertyMetadata(target)
    if (properties != null) {
        properties.forEach((value: MappingRules<T>, key: string) => {
            const data = from[value.mapFromProperty]
            if (data == null) {
                if (!value.isRequired) {
                    return
                }
                throw new MapperError(
                    `Invalid source object: '${value.mapFromProperty}' not returned by the query`
                )
            }
            const expectedType = value.type
            const isPrimitive = primitiveType.has(expectedType.name)
            if (isPrimitive) {
                validate(data, expectedType, value.validator)
                obj[key] = data
            } else {
                obj[key] = consturctNonPrimitiveDataType(data, expectedType)
            }
        })
    }
    return obj
}

function consturctNonPrimitiveDataType<T>(data: any, expectedType: Constructor<T>): T {
    try {
        return new (expectedType as any)(data)
    } catch (error) {
        throw new MapperError(
            `Fail to convert ${data} to type ${expectedType.name}: ${error.message}`
        )
    }
}

function validate<T>(value: any, expectedType: Constructor<T>, validator?: (value: any) => void) {
    if (value.constructor != expectedType) {
        throw new MapperError(
            `Invalid source object; expect ${value} to be of type ${expectedType.name}, but got ${value.constructor.name} instead`
        )
    }
    if (validator != null) {
        try {
            validator(value)
        } catch (err) {
            throw new MapperError(`Invalid source object; validation for ${value} fails: ${err}`)
        }
    }
}

function registerProperty<T>(
    target: Object,
    propertyName: string,
    fromPropertyName: string,
    propertyType: Constructor<T>,
    isRequired: boolean,
    validator?: (value: any) => void
): void {
    const metadata = Reflect.getMetadata('metadata', target.constructor)
    const description: MappingRules<T> = {
        mapFromProperty: fromPropertyName,
        type: propertyType,
        isRequired: isRequired,
        validator: validator,
    }
    if (metadata != null) {
        metadata.set(propertyName, description)
    } else {
        Reflect.defineMetadata(
            'metadata',
            new Map([[propertyName, description]]),
            target.constructor
        )
    }
}

function getPropertyMetadata(target: Object): any {
    return Reflect.getMetadata('metadata', target)
}

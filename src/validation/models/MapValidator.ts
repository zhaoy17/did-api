import { map } from '../../mapper'
import { PrimitiveValidator, Validator } from './Validator'

export class MapValidator {
    @map('length')
    length: number

    @map('item-type')
    itemType: Validator

    @map('key-type')
    keyType: PrimitiveValidator

    constructor(length: number, itemType: Validator, keyType: PrimitiveValidator) {
        this.length = length
        this.itemType = itemType
        this.keyType = keyType
    }

    async validate(value: any): Promise<void> {
        throw new Error('not implemented')
    }
}

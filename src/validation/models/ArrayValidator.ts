import { map } from '../../mapper'
import { Validator } from './Validator'

export class ArrayValidator {
    @map('length')
    length: number

    @map('item-type')
    itemType?: Validator

    @map('unique')
    unique?: boolean

    constructor(length: number, itemType: Validator, unique: boolean) {
        this.length = length
        this.itemType = itemType
        this.unique = unique
    }

    async validate(value: any): Promise<void> {
        throw new Error('not implemented')
    }
}

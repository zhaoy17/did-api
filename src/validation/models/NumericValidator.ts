/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
/* eslint-disable @typescript-eslint/no-explicit-any */

import { map } from '../../mapper'

export class NumericValidator {
    @map('minimum')
    minimum: number

    @map('maximum')
    maximum: number

    @map('oneof')
    oneof: number[]

    constructor(minimum: number, maximum: number, oneof: number[]) {
        this.minimum = minimum
        this.maximum = maximum
        this.oneof = oneof
    }

    async validate(value: any): Promise<void> {
        setTimeout(() => {
            if (typeof value == 'number') {
                if (this.minimum != null) {
                    if (value < this.minimum) {
                        throw new Error(`expect ${value} > ${this.minimum}`)
                    }
                }
                if (this.maximum != null) {
                    if (value > this.maximum) {
                        throw new Error(`expect ${value} < ${this.maximum}`)
                    }
                }
                if (this.oneof != null) {
                    let isOneOf = false
                    for (const val of this.oneof) {
                        if (value == val) {
                            isOneOf = true
                        }
                    }
                    if (!isOneOf) {
                        throw new Error(`expect ${value} to be one of ${this.oneof}`)
                    }
                }
            } else {
                throw new Error(`expect value to be string, but got ${typeof value} instad`)
            }
        }, 0)
    }
}

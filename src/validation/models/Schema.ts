import { map } from '../../mapper'

export class Schema {
    @map('schema_id')
    id: number

    @map('schema_name')
    schemaName: string

    @map('schema')
    schema: string

    @map('extended_by_id')
    parent_id: number

    constructor(schema_name: string, schema: string, parent_id: number, id?: number) {
        this.id = id == null ? -1 : id
        this.schemaName = schema_name
        this.schema = schema
        this.parent_id = parent_id
    }
}

import { map } from '../../mapper'

export class FloatValidator {
    @map('minimum')
    minimum: number

    @map('maximum')
    maximum: number

    @map('oneof')
    oneof: number[]

    constructor(minimum: number, maximum: number, oneof: number[]) {
        this.minimum = minimum
        this.maximum = maximum
        this.oneof = oneof
    }

    async validate(value: any): Promise<void> {
        throw new Error('not implemented')
    }
}

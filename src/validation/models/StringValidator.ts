/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
/* eslint-disable @typescript-eslint/no-explicit-any */

import { map } from '../../mapper'

export class StringValidator {
    @map('length')
    length?: number

    @map('regex')
    regex?: string

    constructor(length: number, regex: string) {
        this.length = length
        this.regex = regex
    }

    async validate(value: any): Promise<void> {
        setTimeout(() => {
            if (typeof value == 'string') {
                if (this.length != null) {
                    if (value.length != this.length) {
                        throw new Error(
                            `expect string len(${value})=${this.length}, but is ${value.length}`
                        )
                    }
                    if (this.regex != null) {
                        const regex = new RegExp(this.regex)
                        if (!regex.test(value)) {
                            throw new Error(`string ${value} does not match regex ${this.regex}`)
                        }
                    }
                }
            } else {
                throw new Error(`expect value to be string, but got ${typeof value} instad`)
            }
        }, 0)
    }
}

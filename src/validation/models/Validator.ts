import { ArrayValidator } from './ArrayValidator'
import { FloatValidator } from './FloatValidator'
import { IntegerValidator } from './IntegerValidator'
import { MapValidator } from './MapValidator'
import { NumericValidator } from './NumericValidator'
import { StringValidator } from './StringValidator'

export type PrimitiveValidator =
    | ArrayValidator
    | IntegerValidator
    | NumericValidator
    | StringValidator
    | FloatValidator

export type Validator = PrimitiveValidator | ArrayValidator | MapValidator

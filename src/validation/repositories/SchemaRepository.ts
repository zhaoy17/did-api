import { ConfigurationNotSupportedError } from '../../app'
import { Driver } from '../../database/drivers/Driver'
import { Schema } from '../models/Schema'
import { SchemaStatements } from './statements/SchemaStatements'
import { Tree } from '../../data-structures/tree'

export class SchemaRepository {
    driver: Driver
    statements: SchemaStatements | null = null

    constructor(driver: Driver) {
        this.driver = driver
    }

    async getSchema(schemaId: number): Promise<Tree<Schema>> {
        const query = (await this.acquireStatements()).getSchema(schemaId)
        const results = await this.driver.query(query[0], query.slice(1), Schema)
        if (results.length != 0) {
            return this.parseResultIntoTree(results)
        } else {
            throw new Error('schemaId does not exists')
        }
    }

    async insertSchema(
        schemaName: string,
        schema: string,
        extendsSchemaId: number[]
    ): Promise<void> {
        const queryToInsert = (await this.acquireStatements()).insertSchema(schemaName, schema)
        const transactionRunner = await this.driver.startTransaction()
        try {
            const result = transactionRunner.execute(queryToInsert[0], queryToInsert.slice(1))
            if (Array.isArray(result) && result.length != 0) {
                const id = result[0].schema_id
                if (id == undefined) {
                    throw new Error('schema insertion did not return a valid schema id')
                }
                const queryToExtend = (await this.acquireStatements()).setSchemaInherentance(
                    id,
                    extendsSchemaId
                )
                await transactionRunner.execute(queryToExtend[0], queryToExtend.slice(1))
                await transactionRunner.commit()
            } else {
                throw new Error('schema insertion did not return schema id')
            }
        } catch (err) {
            await transactionRunner.rollback()
            throw err
        }
    }

    async updateSchema(schemaId: number, schema: string): Promise<void> {
        const queryToUpdate = (await this.acquireStatements()).updateSchemaContent(schemaId, schema)
        await this.driver.execute(queryToUpdate[0], queryToUpdate.slice(1))
    }

    async deleteSchema(schemaId: number): Promise<void> {
        const queryToDelete = (await this.acquireStatements()).deleteSchema(schemaId)
        const queryToCheckDependencies = (await this.acquireStatements()).hasChildren(schemaId)
        const checkDependenciesResult = this.driver.execute(
            queryToCheckDependencies[0],
            queryToCheckDependencies.slice(1)
        )
        if (Array.isArray(checkDependenciesResult) && checkDependenciesResult.length > 0) {
            const hasDependencies: boolean = checkDependenciesResult[0]['exists']
            if (hasDependencies == undefined) {
                throw new Error('fail to check dependencies before deletion')
            }
            if (hasDependencies) {
                throw new Error('cannot delete, it is extended by another schema')
            }
            await this.driver.execute(queryToDelete[0], queryToDelete.slice(1))
        }
        throw new Error('fail to check dependencies before deletion')
    }

    async getSchemaByName(schemaName: string): Promise<Tree<Schema>> {
        const query = (await this.acquireStatements()).getSchemaByName(schemaName)
        const results = await this.driver.query(query[0], query.slice(1), Schema)
        if (results.length != 0) {
            return this.parseResultIntoTree(results)
        } else {
            throw new Error('schemaId does not exists')
        }
    }

    private parseResultIntoTree(results: Schema[]) {
        const tree = new Tree(results[0], results[0].id.toString())
        for (const schema of results) {
            tree.addChild(schema.parent_id.toString(), schema.id.toString(), schema)
        }
        return tree
    }

    private async acquireStatements(): Promise<SchemaStatements> {
        if (this.statements == null) {
            if (this.driver.driverName == 'postgres') {
                this.statements = new (
                    await import('./statements/SchemaPostgresStatements')
                ).SchemaPostgresStatements()
            } else {
                throw new ConfigurationNotSupportedError(
                    `schema operations do not support ${this.driver.driverName}`
                )
            }
        }
        return this.statements
    }
}

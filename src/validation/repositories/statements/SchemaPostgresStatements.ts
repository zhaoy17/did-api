import { SchemaStatements } from './SchemaStatements'

export class SchemaPostgresStatements implements SchemaStatements {
    getSchema(schemaId: number): string[] {
        return [
            `WITH RECURSIVE children AS (
                SELECT s.schema_id, s.schema_name, s."schema", i.extends_schema_id 
                FROM "schemas" AS s, schemainherentance AS i
                WHERE s.schema_id = i.extended_by_id
                AND s.schema_id = $1
            UNION ALL
                SELECT s.schema_id, s.schema_name, s."schema", c.extends_schema_id
                FROM "schemas" AS s, schemainherentance AS i, children as c
                WHERE s.schema_id = i.extended_by_id
                AND s.schema_id = c.schema_id
            )
            SELECT * FROM children;`,
            schemaId.toString(),
        ]
    }

    getSchemaByName(schemaName: string): string[] {
        return [
            `WITH RECURSIVE children AS (
                SELECT s.schema_id, s.schema_name, s."schema", i.extends_schema_id 
                FROM "schemas" AS s, schemainherentance AS i
                WHERE s.schema_id = i.extended_by_id
                AND s.schema_name = $1
            UNION ALL
                SELECT s.schema_id, s.schema_name, s."schema", c.extends_schema_id
                FROM "schemas" AS s, schemainherentance AS i, children as c
                WHERE s.schema_id = i.extended_by_id
                AND s.schema_id = c.schema_id
            )
            SELECT * FROM children;`,
            schemaName,
        ]
    }

    getSingleSchemaById(schemaId: number): string[] {
        return [
            `
            SELECT s.schema_id, s.schema_name, s."schema", i.extends_schema_id 
            FROM "schemas" AS s, schemainherentance AS i
            WHERE s.schema_id = i.schema_id
            AND s.schema_name = $1`,
            schemaId.toString(),
        ]
    }

    getSingleSchemaByName(schemaName: string): string[] {
        return [
            `
            SELECT s.schema_id, s.schema_name, s."schema", i.extends_schema_id 
            FROM "schemas" AS s, schemainherentance AS i
            WHERE s.schema_id = i.schema_id
            AND s.schema_id = $1`,
            schemaName,
        ]
    }

    insertSchema(schemaName: string, schema: string): string[] {
        return [
            `
            INSERT INTO schemas(schema_name, schema)
            VALUES ($1, $2)
            RETURNING schema_id
            `,
            schemaName,
            schema.toString(),
        ]
    }

    setSchemaInherentance(schemaId: number, extendsSchemaId: number[]): string[] {
        let values = ''
        const schemasInherentanceList = []
        for (let i = 0; i < extendsSchemaId.length; i++) {
            values += `($${i * 2 + 1}, $${i * 2 + 2})\n`
            schemasInherentanceList.push(extendsSchemaId[i].toString())
            schemasInherentanceList.push(schemaId.toString())
        }
        return [
            `
            INSERT INTO schemainherentance(schema_id, extended_by_id)
            VALUES ${values}
            `,
            ...schemasInherentanceList,
        ]
    }

    updateSchemaContent(schemaId: number, newContent: string): string[] {
        return [
            `
            UPDATE schemas
            SET schema = $1
            WEHERE schema_id = $2
            `,
            newContent,
            schemaId.toString(),
        ]
    }

    hasChildren(schemaId: number): string[] {
        return [
            `
            SELECT EXISTS(
                SELECT * 
                FROM schemainherentance
                WHERE schema_id = $1
            )`,
            schemaId.toString(),
        ]
    }

    deleteSchema(schemaId: number): string[] {
        return [
            `
            DELETE FROM schemas AS s
            WHERE s.schema_id = $1
            `,
            schemaId.toString(),
        ]
    }
}

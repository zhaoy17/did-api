export interface SchemaStatements {
    getSchema(schemaId: number): string[]

    getSchemaByName(schemaName: string): string[]

    getSingleSchemaById(schemaId: number): string[]

    getSingleSchemaByName(schemaName: string): string[]

    insertSchema(schemaName: string, schema: string): string[]

    setSchemaInherentance(schemaId: number, extendsSchemaId: number[]): string[]

    updateSchemaContent(schemaId: number, newContent: string): string[]

    hasChildren(schemaId: number): string[]

    deleteSchema(schemaId: number): string[]
}

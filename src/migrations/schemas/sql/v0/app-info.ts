import { TransactionRunner } from '../../../../database/drivers/TransactionRunner'

const CREATE_APP_INFO_TABLE = `
    CREATE TABLE IF NOT EXISTS appinfo (
        time_created TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP,
        did_version NUMERIC NOT NULL,
        dbschema_version NUMERIC NOT NULL,
        PRIMARY KEY (did_version, dbschema_version)
    )
`

const INSERT_VERSION_INTO_TABLE = `
    INSERT INTO appinfo (did_version, dbschema_version)
    VALUES (0, 0)
`

const DROP_APP_INFO_TABLE = `
    DROP TABLE appinfo;
`

export async function apply(transactionRunner: TransactionRunner): Promise<void> {
    if (transactionRunner.driverName == 'postgres') {
        await transactionRunner.execute(CREATE_APP_INFO_TABLE, [])
        await transactionRunner.execute(INSERT_VERSION_INTO_TABLE, [])
    }
}

export async function revert(transactionRunner: TransactionRunner): Promise<void> {
    if (transactionRunner.driverName == 'postgres') {
        await transactionRunner.execute(DROP_APP_INFO_TABLE, [])
    }
}

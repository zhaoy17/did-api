import { TransactionRunner } from '../../../../database/drivers/TransactionRunner'

const CREATE_DOCUMENT_TABLE = `
    CREATE TABLE documents 
    (
        document_id varchar NOT NULL,
        document_version varchar NOT NULL,
        schema_id varchar NOT NULL,
        schema_version varchar NOT NULL,
        "document" jsonb NOT NULL,
        time_created timestamp NOT NULL,
        CONSTRAINT documents_documentid_documentversion_pk PRIMARY KEY (document_id, document_version)
    );
    
    CREATE UNIQUE INDEX ON documents USING btree (document_version, document_id);
    CREATE INDEX ON documents USING btree (document_version, document);

    ALTER TABLE documents 
    ADD CONSTRAINT document_schemaid_fk FOREIGN KEY (schema_id, schema_version) 
    REFERENCES schemas(schema_id, schema_version) ON DELETE RESTRICT;
`

const DROP_DOCUMENT_TABLE = `
    DROP TABLE documents
`

const CREATE_INHERENTANCE_TABLE = `
    CREATE TABLE documentsinherentance 
    (
        document_version varchar NOT NULL,
        document_id varchar NOT NULL,
        inhereted_from_document_version varchar NOT NULL,
        inherented_from_document_id varchar NOT NULL,
        CONSTRAINT documentsinherentance_documentversion_documentid_pk PRIMARY KEY (document_version, document_id)
    );

    CREATE INDEX ON public.documentsinherentance USING btree (document_version, document_id);
    CREATE INDEX ON public.documentsinherentance USING btree (inhereted_from_document_version, inherented_from_document_id);

    ALTER TABLE public.documentsinherentance 
    ADD CONSTRAINT documentsinherentance_documentversion_documentid_fk FOREIGN KEY (document_version,document_id) 
    REFERENCES documents(document_version,document_id);
    
    ALTER TABLE public.documentsinherentance 
    ADD CONSTRAINT documentsinherentance_inherentedfromversion_inherentedfromid_fk FOREIGN KEY (inhereted_from_document_version,inherented_from_document_id)
    REFERENCES documents(document_version,document_id) ON DELETE RESTRICT;
`

const DROP_INHERENTANCE_TABLE = `
    DROP TABLE documentsinherentance
`

const CREATE_DEPENDENCIES_TABLE = `
    CREATE TABLE documentsdependencies 
    (
        document_version varchar NOT NULL,
        document_id varchar NOT NULL,
        depended_by_document_version varchar NULL,
        depended_by_document_id varchar NULL,
        CONSTRAINT documentsdependencies_versionid_pk PRIMARY KEY (document_version, document_id)
    );

    CREATE INDEX ON public.documentsdependencies USING btree (document_version, document_id);
    CREATE INDEX ON public.documentsdependencies USING btree (depended_by_document_version, depended_by_document_id);

    ALTER TABLE public.documentsdependencies 
    ADD CONSTRAINT documentsdependencies_documentversion_documentid_fk FOREIGN KEY (document_version,document_id) 
    REFERENCES documents(document_version,document_id);
    
    ALTER TABLE public.documentsdependencies 
    ADD CONSTRAINT documentdependencies_documentversion_dependedby_fk FOREIGN KEY (depended_by_document_version,depended_by_document_id)
    REFERENCES documents(document_version,document_id) ON DELETE RESTRICT;
`

const DROP_DEPENDENCIES_TABLE = `
    DROP TABLE documentsdependencies
`

export async function apply(transactionRunner: TransactionRunner): Promise<void> {
    if (transactionRunner.driverName == 'postgres') {
        await transactionRunner.execute(CREATE_DOCUMENT_TABLE, [])
        await Promise.all([
            transactionRunner.execute(CREATE_INHERENTANCE_TABLE, []),
            transactionRunner.execute(CREATE_DEPENDENCIES_TABLE, []),
        ])
    }
}

export async function revert(transactionRunner: TransactionRunner): Promise<void> {
    if (transactionRunner.driverName == 'postgres') {
        await Promise.all([
            await transactionRunner.execute(DROP_DEPENDENCIES_TABLE, []),
            await transactionRunner.execute(DROP_INHERENTANCE_TABLE, []),
        ])
        await transactionRunner.execute(DROP_DOCUMENT_TABLE, [])
    }
}

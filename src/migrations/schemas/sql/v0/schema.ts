import { TransactionRunner } from '../../../../database/drivers/TransactionRunner'

const CREATE_SCHEMA_TABLE = `
    CREATE TABLE "schemas" 
    (
        schema_id varchar NOT NULL,
        schema_name varchar NOT NULL,
        "schema" varchar NOT NULL,
        schema_version varchar NOT NULL,
        CONSTRAINT schemas_schemaid_pk PRIMARY KEY (schema_id, schema_version)
    );
    CREATE INDEX ON "schemas" USING btree (schema_id, schema_version);
`

const DROP_SCHEMA_TABLE = `
    DROP TABLE "schema";
`

const CREATE_SCHEMA_INHERENTANCE_TABLE = `
    CREATE TABLE schemainherentance 
    (
        schema_id varchar NOT NULL,
        schema_version varchar NOT NULL,
        extended_by_id varchar,
        extended_by_version varchar NOT NULL
    );

    ALTER TABLE schemainherentance 
    ADD CONSTRAINT schemainherentance_schemaid_fk FOREIGN KEY (schema_id, schema_version) 
    REFERENCES "schemas"(schema_id, schema_version) ON DELETE CASCADE;
    
    ALTER TABLE schemainherentance 
    ADD CONSTRAINT schemainherentance_extendebyid_fk FOREIGN KEY (extended_by_id, extended_by_version) 
    REFERENCES "schemas"(schema_id, schema_version) ON DELETE RESTRICT;
`

const DROP_SCHEMA_INHERENTANCE_TABLE = `
    DROP TABLE schemainherentance;
`

export async function apply(transactionRunner: TransactionRunner): Promise<void> {
    if (transactionRunner.driverName == 'postgres') {
        await transactionRunner.execute(CREATE_SCHEMA_TABLE, [])
        await transactionRunner.execute(CREATE_SCHEMA_INHERENTANCE_TABLE, [])
    }
}

export async function revert(transactionRunner: TransactionRunner): Promise<void> {
    if (transactionRunner.driverName == 'postgres') {
        await transactionRunner.execute(DROP_SCHEMA_INHERENTANCE_TABLE, [])
        await transactionRunner.execute(DROP_SCHEMA_TABLE, [])
    }
}

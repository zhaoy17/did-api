import Migration from '../../../Migration'
import * as documents from './documents'
import * as appinfo from './app-info'
import * as versioning from './versioning'
import * as schema from './schema'
import { TransactionRunner } from '../../../../database/drivers/TransactionRunner'

export default class SchemaV0 implements Migration {
    async createTables(transactionRunner: TransactionRunner): Promise<void> {
        await appinfo.apply(transactionRunner)
        await versioning.apply(transactionRunner)
        await schema.apply(transactionRunner)
        await documents.apply(transactionRunner)
    }

    async up(transactionRunner: TransactionRunner): Promise<void> {
        await this.createTables(transactionRunner)
    }

    async down(transactionRunner: TransactionRunner): Promise<void> {
        await documents.revert(transactionRunner)
        await schema.revert(transactionRunner)
        await versioning.revert(transactionRunner)
        await appinfo.revert(transactionRunner)
    }
}

import { TransactionRunner } from '../../../../database/drivers/TransactionRunner'

const CREATE_BRNACH_TABLE = `
    CREATE TABLE branches
    (
        branch_id varchar NOT NULL,
        commit_id varchar NOT NULL,
        current_working_snapshot_id varchar NOT NULL,
        CONSTRAINT branches_branchid_pk PRIMARY KEY (branch_id)
    );

    ALTER TABLE public.branches
    ADD CONSTRAINT branhes_commitid_fk FOREIGN KEY (commit_id)
    REFERENCES commits(commit_id) ON DELETE RESTRICT;
`

const DROP_BRANCH_TABLE = `
    DROP TABLE branches;
`

const CREATE_COMMITS_TABLE = `
    CREATE TABLE commits
    (
        commit_id varchar NOT NULL,
        last_commit_id varchar NOT NULL,
        snapshot_id varchar NULL,
        message varchar NULL,
        time_created timestamp NOT NULL,
        created_by varchar NOT NULL,
        CONSTRAINT commits_commitid_pk PRIMARY KEY (commit_id)
    );

    ALTER TABLE public.commits
    ADD CONSTRAINT commits_last_commit_id_fk FOREIGN KEY (last_commit_id)
    REFERENCES commits(commit_id) ON DELETE RESTRICT;
`

const DROP_COMMITS_TABLE = `
    DROP TABLE commits;
`

const CREATE_SERVICE_TABLE = `
    CREATE TABLE services
    (
        service_id int4 NOT NULL,
        description varchar NOT NULL,
        CONSTRAINT "servies_serviceid_pk" PRIMARY KEY (service_id)
    );
`

const DROP_SERVICE_TABLE = `
    DROP TABLE services;
`

const CREATE_PR_TABLE = `
    CREATE TABLE pullrequests
    (
        request_id int4 NOT NULL,
        source_branch_id varchar NOT NULL,
        CONSTRAINT pullrequests_requestid_pk PRIMARY KEY (request_id)
    );

    ALTER TABLE pullrequests
    ADD CONSTRAINT pullrequests_sourcebranchid_fk FOREIGN KEY (source_branch_id)
    REFERENCES branches(branch_id);
`

const DROP_PR_TABLE = `
    DROP TABLE pullrequests;
`

const CREATE_COMMENTS_TABLE = `
    CREATE TABLE "comments"
    (
        comment_id int4 NOT NULL,
        request_id int4 NOT NULL,
        topic varchar NOT NULL,
        message varchar NOT NULL,
        CONSTRAINT comment_commentid_pk PRIMARY KEY (comment_id)
    );

    ALTER TABLE "comments" ADD CONSTRAINT pullrequests_requestid_fk
    FOREIGN KEY (request_id) REFERENCES pullrequests(request_id);
`

const DROP_COMMENTS_TABLE = `
    DROP TABLE "comments";
`

const CREATE_SNAPSHOT_TABLE = `
    CREATE TABLE snapshots
    (
        snapshot_id varchar NOT NULL,
        version varchar NOT NULL,
        id varchar NOT NULL,
        service_id int4 NOT NULL,
        CONSTRAINT documentsnapshot_documentversion_documentid_pk PRIMARY KEY (version, id)
    );
    CREATE UNIQUE INDEX ON snapshots USING btree (snapshot_id, version, id, service_id);

    ALTER TABLE snapshots
    ADD CONSTRAINT snapshots_serviceid_fk FOREIGN KEY (service_id)
    REFERENCES services(service_id);
`

const DROP_SNAPSHOT_TABLE = `
    DROP TABLE snapshots;
`

export async function apply(transactionRunner: TransactionRunner): Promise<void> {
    if (transactionRunner.driverName == 'postgres') {
        await transactionRunner.execute(CREATE_SERVICE_TABLE, [])
        await transactionRunner.execute(CREATE_SNAPSHOT_TABLE, [])
        await transactionRunner.execute(CREATE_COMMITS_TABLE, [])
        await transactionRunner.execute(CREATE_BRNACH_TABLE, [])
        await transactionRunner.execute(CREATE_PR_TABLE, [])
        await transactionRunner.execute(CREATE_COMMENTS_TABLE, [])
    }
}

export async function revert(transactionRunner: TransactionRunner): Promise<void> {
    if (transactionRunner.driverName == 'postgres') {
        await transactionRunner.execute(DROP_COMMENTS_TABLE, [])
        await transactionRunner.execute(DROP_PR_TABLE, [])
        await transactionRunner.execute(DROP_BRANCH_TABLE, [])
        await transactionRunner.execute(DROP_COMMITS_TABLE, [])
        await transactionRunner.execute(DROP_SNAPSHOT_TABLE, [])
        await transactionRunner.execute(DROP_SERVICE_TABLE, [])
    }
}

import { TransactionRunner } from '../database/drivers/TransactionRunner'

export default interface Migration {
    createTables(transactionRunner: TransactionRunner): Promise<void>

    up(queryRunner: TransactionRunner): Promise<void>

    down(queryRunner: TransactionRunner): Promise<void>
}

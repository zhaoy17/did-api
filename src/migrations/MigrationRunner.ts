import { Driver } from '../database/drivers/Driver'
import { TransactionRunner } from '../database/drivers/TransactionRunner'
import SchemaV0 from './schemas/sql/v0'
import Migration from './Migration'

const LATEST_DB_VERSION = 0

const NEXT_VERSION_MAP = new Map([[-1, 0]])

const SCHEMA_MAP = new Map([[-1, new SchemaV0()]])

export default class MigrationRunner {
    driver: Driver
    dbVersions: DbVersions

    constructor(driver: Driver, dbVersions?: DbVersions) {
        this.driver = driver
        if (dbVersions == undefined) {
            this.dbVersions = new DbVersions(LATEST_DB_VERSION, NEXT_VERSION_MAP, SCHEMA_MAP)
        } else {
            this.dbVersions = dbVersions
        }
    }

    async applyMigration(): Promise<void> {
        const dbVersion = await this.getCurrentDbSchemaVersion()

        if (dbVersion != this.dbVersions.latestDbVersion) {
            const transactionRunner = await this.driver.startTransaction()
            try {
                if (dbVersion == -1) {
                    await this.createTablesFromScratch(transactionRunner)
                } else {
                    await this.migrateFromVersion(transactionRunner, dbVersion)
                }
                await transactionRunner.commit()
            } catch (err) {
                await transactionRunner.rollback()
                throw new MigrationError(`Driver error ${err}`)
            }
        }
    }

    async getCurrentDbSchemaVersion(): Promise<number> {
        const migrationTableExists = await this.migrationTableExists()

        if (!migrationTableExists) {
            return -1
        }
        let result = []

        if (this.driver.driverName == 'postgres') {
            result = await this.driver.execute('SELECT * from appinfo', [])
        } else {
            throw new Error('driver is not supported')
        }
        if (Array.isArray(result) && result.length > 0) {
            const version = result[0].dbschema_version
            if (version == undefined) {
                throw new MigrationError('Migration table invalid')
            }
            return version
        }
        throw new MigrationError('Empty migration table')
    }

    private nextVersion(version: number): number {
        if (version == this.dbVersions.latestDbVersion) {
            return version
        }
        const nextVersion = this.dbVersions.nextVersionMap.get(version)

        if (nextVersion == undefined) {
            throw new MigrationError(`Version ${version} does not exist`)
        }
        return nextVersion
    }

    private async migrateFromVersion(
        transactionRunner: TransactionRunner,
        dbVersion: number
    ): Promise<void> {
        while (dbVersion < this.dbVersions.latestDbVersion) {
            const migration = this.dbVersions.versionMigrationMap.get(dbVersion)
            if (migration !== undefined) {
                await migration.up(transactionRunner)
                dbVersion = this.nextVersion(dbVersion)
            } else {
                throw new MigrationError(`dbVersion ${dbVersion} does not exist`)
            }
        }
    }

    private async createTablesFromScratch(transactionRunner: TransactionRunner): Promise<void> {
        const migration = this.dbVersions.versionMigrationMap.get(-1)
        if (migration !== undefined) {
            await migration.createTables(transactionRunner)
        }
    }

    public async migrationTableExists(): Promise<boolean> {
        if (this.driver.driverName == 'postgres') {
            const result = await this.driver.execute(
                `
                SELECT EXISTS (
                    SELECT FROM information_schema.tables
                    WHERE table_name = 'appinfo'
                )
            `,
                []
            )
            if (Array.isArray(result) && result.length == 1) {
                if (typeof result[0]['exists'] == 'boolean') {
                    return result[0]['exists']
                }
                throw new MigrationError('unable to determine if migration table exists')
            } else {
                throw new MigrationError('unable to determine if migration table exists')
            }
        } else {
            throw new MigrationError('driver is not supported')
        }
    }
}

export class MigrationError extends Error {
    constructor(message?: string) {
        super(message)
        Object.setPrototypeOf(this, MigrationError.prototype)
    }
}

export class DbVersions {
    latestDbVersion: number
    nextVersionMap: Map<number, number>
    versionMigrationMap: Map<number, Migration>

    constructor(
        latestDbVersion: number,
        nextVersionMap: Map<number, number>,
        versionMigrationMap: Map<number, Migration>
    ) {
        this.latestDbVersion = latestDbVersion
        this.nextVersionMap = nextVersionMap
        this.versionMigrationMap = versionMigrationMap
    }
}

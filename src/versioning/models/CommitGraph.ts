import Commit from './Commit'

export default interface CommitGraph {
    commit: Commit
    children?: Commit[]
    parent?: Commit
}

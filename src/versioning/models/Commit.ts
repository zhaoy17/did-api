export default interface CommitGraph {
    commitId: string
    snapshotId: string
    message: string
    branchName: string
}

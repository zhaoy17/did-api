export default interface Branch {
    branchId: string
    branchName: string
    commitId: string
    currentWorkingSnapshot: string
}

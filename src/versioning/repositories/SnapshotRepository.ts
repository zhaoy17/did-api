export default interface SnapshotRepository {
    insertSnapshot(snapshotId: string, hash: string): Promise<string>

    deleteSnapshot(snapshotId: string, hash: string): Promise<string>

    createNewSnapshotBasedOfOff(snapshotId: string): Promise<string>

    updateSnapshotHashValue(
        snapshotId: string,
        oldHashValue: string,
        newHashValue: string
    ): Promise<string[]>

    getAllHashes(snapshotId: string): Promise<Set<string>>
}

import Branch from '../models/Branch'

export default interface BranchRepository {
    createNewBranch(
        branchId: string,
        branchName: string,
        commitId: string,
        currentWorkingSnapshotId: string
    ): Promise<string>

    deleteBranch(branchId: string): Promise<string>

    getCommitId(branchId: string): Promise<string>

    getCurrentWorkingSnapshotId(branchId: string): Promise<string>

    updateCommitId(branchId: string): Promise<string>

    updateCurrentWorkingSnapshotId(branchId: string): Promise<string>

    updateBranchName(branchName: string): Promise<string>

    getBranch(branchId: string): Branch

    getBranchByName(branchName: string): Branch
}

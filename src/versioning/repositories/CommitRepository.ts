import CommitGraph from '../models/CommitGraph'
import Commit from '../models/Commit'

export default interface CommitRepository {
    getCommitById(commitId: string): Promise<Commit>

    getCommitsBetweenTimestamp(startTimestamp: string, endTimestamp: string): Promise<CommitGraph>

    getCommitsAfterTimestamp(endTimestamp: string): Promise<CommitGraph>

    getSnapshotId(commitId: string): Promise<string>

    createNewCommit(
        commitId: string,
        snapshotId: string,
        message: string,
        previousCommitId: string
    ): Promise<string>

    updateSnapshot(commitId: string, snapshotId: string): Promise<string>

    updateMessage(commitId: string, messageId: string): Promise<string>

    updatePreviousCommitId(commitId: string): Promise<string>

    getFullCommitHistory(commitId: string): Promise<CommitGraph>
}

import * as chai from 'chai'
import { expect } from 'chai'
import { assert, createStubInstance, match, spy } from 'sinon'
import chaiAsPromise from 'chai-as-promised'
import MigrationRunner, {
    DbVersions,
    MigrationError,
} from '../../../src/migrations/MigrationRunner'
import Migration from '../../../src/migrations/Migration'
import {
    PostgresDriver,
    PostgresTransactionRunner,
} from '../../../src/database/drivers/PotgresDriver'
import { Driver } from '../../../src/database/drivers/Driver'
import { TransactionRunner } from '../../../src/database/drivers/TransactionRunner'

chai.use(chaiAsPromise)

class MockSchema implements Migration {
    throwError = false

    constructor(throwError?: boolean) {
        if (throwError != null) {
            this.throwError = throwError
        }
    }

    async up() {
        if (this.throwError) {
            throw new Error()
        }
        return undefined
    }

    async down() {
        return undefined
    }

    async createTables() {
        return undefined
    }
}

describe('Tests for migration function', function () {
    describe('get database version from migration table', function () {
        const mockDbVersion = new DbVersions(-1, new Map(), new Map())

        it('should throw error if migration table is empty', async function () {
            const mockDriver = createStubInstance(PostgresDriver)
            mockDriver.execute.withArgs(match.any, match.any).resolves([])
            mockDriver.driverName = 'postgres'

            const migrationRunner = new MigrationRunner(mockDriver as Driver, mockDbVersion)
            migrationRunner.migrationTableExists = async () => true

            return expect(migrationRunner.getCurrentDbSchemaVersion()).to.eventually.rejectedWith(
                MigrationError
            )
        })

        it('should return a valid version number if migration table is not empty', async function () {
            const mockDriver = createStubInstance(PostgresDriver)
            mockDriver.execute.withArgs(match.any, match.any).resolves([{ dbschema_version: 3 }])
            mockDriver.driverName = 'postgres'

            const migrationRunner = new MigrationRunner(mockDriver as Driver, mockDbVersion)
            migrationRunner.migrationTableExists = async () => true

            return expect(migrationRunner.getCurrentDbSchemaVersion()).to.eventually.equals(3)
        })

        it('should return -1 if migration table does not exist', async function () {
            const mockDriver = createStubInstance(PostgresDriver)
            mockDriver.driverName = 'postgres'

            const migrationRunner = new MigrationRunner(mockDriver as Driver, mockDbVersion)
            migrationRunner.migrationTableExists = async () => false

            return expect(migrationRunner.getCurrentDbSchemaVersion()).to.eventually.equals(-1)
        })
    })

    describe('migrate from past version tests', function () {
        const mockTransactionRunner = createStubInstance(PostgresTransactionRunner)
        mockTransactionRunner.driverName = 'postgres'

        const mockDriver = createStubInstance(PostgresDriver)
        mockDriver.startTransaction.resolves(mockTransactionRunner as TransactionRunner)

        it('should apply migration for each version in the correct order', async function () {
            const schemaV1Dot0 = new MockSchema()
            const schemaV1Dot5 = new MockSchema()
            const schemaV2Dot0 = new MockSchema()
            const schemaV3Dot0 = new MockSchema()

            const nextVersionMap = new Map<number, number>([
                [1.0, 1.5],
                [1.5, 2.0],
                [2.0, 3.0],
            ])

            const versionMigrationMap = new Map<number, Migration>([
                [-1, schemaV1Dot0],
                [1.0, schemaV1Dot5],
                [1.5, schemaV2Dot0],
                [2.0, schemaV3Dot0],
            ])

            const mockDbVersion = new DbVersions(3.0, nextVersionMap, versionMigrationMap)

            const spies = [
                spy(schemaV1Dot0, 'up'),
                spy(schemaV1Dot5, 'up'),
                spy(schemaV2Dot0, 'up'),
                spy(schemaV3Dot0, 'up'),
            ]

            const migrationRunner = new MigrationRunner(mockDriver as Driver, mockDbVersion)
            migrationRunner.getCurrentDbSchemaVersion = async () => 1.0

            await migrationRunner.applyMigration()
            assert.callOrder(spies[1], spies[2], spies[3])
        })

        it('should throw MigrationError and rollback transaction if migration fails', async function () {
            const badSchemaVersion = new MockSchema(true)

            const nextVersionMap = new Map<number, number>([[1.0, 2.0]])
            const versionMigrationMap = new Map<number, Migration>([[1.0, badSchemaVersion]])

            const mockDbVersion = new DbVersions(2.0, nextVersionMap, versionMigrationMap)

            const migrationRunner = new MigrationRunner(mockDriver as Driver, mockDbVersion)
            migrationRunner.getCurrentDbSchemaVersion = async () => 1.0

            try {
                mockTransactionRunner.rollback.reset()
                await migrationRunner.applyMigration()
                expect.fail()
            } catch (err) {
                expect(err instanceof MigrationError).to.be.true
                assert.calledOnce(mockTransactionRunner.rollback)
            }
        })
    })
})

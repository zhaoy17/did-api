import * as chai from 'chai'
import { expect } from 'chai'
import chaiAsPromise from 'chai-as-promised'
import { map, mapFromTo, MapperError } from '../../src/mapper'

chai.use(chaiAsPromise)

describe('Tests for mapper', function () {
    describe('simple mapping', function () {
        class Example {
            @map('fieldA')
            a: string

            @map('fieldB')
            b: number

            @map('fieldC')
            c: Record<string, unknown>

            @map('fieldD')
            d: string

            constructor(a: string, b: number, c: Record<string, unknown>, d: string) {
                this.a = a
                this.b = b
                this.c = c
                this.d = d
            }
        }

        it('should correct map query result to entity', async function () {
            const mockData = [
                {
                    fieldA: 'string',
                    fieldB: 100,
                    fieldC: { field: 'value' },
                    fieldD: 'string2',
                },
                {
                    fieldA: 'string2',
                    fieldB: 101,
                    fieldC: { field: 'value' },
                    fieldD: 'string3',
                },
            ]
            const results = mockData.map((value) => mapFromTo(value, Example))
            expect(results.length).equals(2)
            expect(results[0]['a']).equals('string')
            expect(results[0]['b']).equals(100)
            expect(results[0]['c']).deep.equals({ field: 'value' })
            expect(results[0]['d']).equals('string2')
            expect(results[1]['a']).equals('string2')
            expect(results[1]['b']).equals(101)
            expect(results[1]['c']).deep.equals({ field: 'value' })
            expect(results[1]['d']).equals('string3')
        })

        it('should throw mapper error if expected fields are not found', async function () {
            const mockData = {
                a: 'string',
                fieldB: 100,
                fieldC: { field: 'value' },
                d: 'string2',
            }
            expect(() => mapFromTo(mockData, Example)).to.throws(MapperError)
        })

        it('should throw mapper error if expected fields has the wrong type', async function () {
            const mockData = {
                fieldA: 'string',
                fieldB: 100,
                fieldC: 200,
                fieldD: 'string2',
            }
            expect(() => mapFromTo(mockData, Example)).to.throws(MapperError)
        })
    })

    describe('complex mapping', function () {
        class B {
            b: string

            constructor(b: string) {
                this.b = b
            }
        }

        class D {
            d: number

            constructor(d: string) {
                const number = Number(d)
                if (isNaN(number)) {
                    throw new Error('Cannot parse number')
                }
                this.d = Number(d)
            }
        }

        class ExampleA {
            @map('fieldB')
            b: B

            @map('fieldD')
            d: D

            constructor(b: B, d: D) {
                this.b = b
                this.d = d
            }
        }

        it('should correctly map query result to entity', async function () {
            const mockData = [
                {
                    fieldB: 'string',
                    fieldD: '100',
                },
                {
                    fieldB: 'string2',
                    fieldD: '102',
                },
            ]
            const results = mockData.map((value) => mapFromTo(value, ExampleA))
            expect(results.length).equals(2)
            expect(results[0].b.b).equals('string')
            expect(results[0].d.d).equals(100)
            expect(results[1].b.b).equals('string2')
            expect(results[1].d.d).equals(102)
        })

        it('should throw mapper error if fail to inject the object', async function () {
            const mockData = [
                {
                    fieldB: 'string',
                    fieldD: '100',
                },
                {
                    fieldB: 'string2',
                    fieldD: 'string',
                },
            ]
            expect(() => mockData.map((value) => mapFromTo(value, ExampleA))).to.throws(MapperError)
        })
    })

    describe('map with optional property', function () {
        class ExampleA {
            @map('fieldB', String, false)
            b?: string

            @map('fieldD', String, false)
            d?: string
        }

        it('should not throw error if optional property cannot be mapped', async function () {
            const mockData = [
                {
                    fieldE: 'string',
                    fieldF: '100',
                },
                {
                    fieldE: 'string2',
                    fieldF: 'string',
                },
            ]
            const results = mockData.map((value) => mapFromTo(value, ExampleA))
            expect(results.length).equals(2)
            expect(results[0].b).equals(undefined)
            expect(results[0].d).equals(undefined)
            expect(results[1].b).equals(undefined)
            expect(results[1].d).equals(undefined)
        })

        it('should throw error if validation fails regardless if it is optional or not', async function () {
            const mockData = [
                {
                    fieldB: 100,
                    fieldD: 200,
                },
                {
                    fieldB: 10,
                    fieldD: 30,
                },
            ]
            expect(() => mockData.map((value) => mapFromTo(value, ExampleA))).to.throws(MapperError)
        })

        describe('test validator', async function () {
            function lenIs6(value: string) {
                if (value.length != 6) {
                    throw new Error('')
                }
            }

            class ExampleA {
                @map('fieldB', String, false, lenIs6)
                b?: string

                @map('fieldD', String, false, lenIs6)
                d?: string
            }

            it('should throw mapper error if validation fails', function () {
                const mockData = [
                    {
                        fieldB: '100',
                        fieldD: '200',
                    },
                    {
                        fieldB: '10',
                        fieldD: '30',
                    },
                ]
                expect(() => mockData.map((value) => mapFromTo(value, ExampleA))).to.throws(
                    MapperError
                )
            })

            it('should correctly map result to object if validation succeeds', async function () {
                const mockData = [
                    {
                        fieldB: '100000',
                        fieldD: '123456',
                    },
                    {
                        fieldB: '103333',
                        fieldD: '305689',
                    },
                ]
                const results = mockData.map((value) => mapFromTo(value, ExampleA))
                expect(results.length).equals(2)
                expect(results[0].b).equals('100000')
                expect(results[0].d).equals('123456')
                expect(results[1].b).equals('103333')
                expect(results[1].d).equals('305689')
            })
        })
    })
})

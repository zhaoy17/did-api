import { Tree, InvalidTreeOperationError } from '../../../src/data-structures/tree'
import { expect } from 'chai'

describe('Tests for tree', function () {
    describe('constructor', function () {
        it('should have root equals to the value passed in', function () {
            const tree = new Tree(3, 'root')
            expect(tree.getNodeData('root')).to.equals(3)
            expect(tree.getRootId()).to.equals('root')
        })
    })

    describe('inserting nodes', function () {
        const tree = new Tree(3, 'root')
        tree.addChild('root', 'node1', 4)
        tree.addChild('root', 'node2', 5)
        tree.addChild('node1', 'node3', 6)
        tree.addChild('node1', 'node4', 12)
        tree.setNodeData('node4', 7)

        it('nodes should have the correct child-parent order', function () {
            expect(tree.isChild('root', 'node1')).to.equals(true)
            expect(tree.isChild('root', 'node2')).to.equals(true)
            expect(tree.isChild('node1', 'node3')).to.equals(true)
            expect(tree.isChild('node1', 'root')).to.equals(false)
            expect(tree.isChild('root', 'node3')).to.equals(false)
        })

        it('should be able to get the correct node data from its id', function () {
            expect(tree.getNodeData('root')).to.equals(3)
            expect(tree.getNodeData('node1')).to.equals(4)
            expect(tree.getNodeData('node2')).to.equals(5)
            expect(tree.getNodeData('node3')).to.equals(6)
            expect(tree.getNodeData('node4')).to.equals(7)
        })

        it('should be able to get correct children id', function () {
            expect(tree.getChildrenId('root')).to.eql(['node1', 'node2'])
            expect(tree.getChildrenId('node1')).to.eql(['node3', 'node4'])
            expect(tree.getChildrenId('node3')).to.eql([])
        })
    })

    describe('traversal', function () {
        const tree = new Tree(1, 'root')
        tree.addChild('root', 'node1', 2)
        tree.addChild('root', 'node2', 3)
        tree.addChild('root', 'node3', 4)
        tree.addChild('node1', 'node4', 5)
        tree.addChild('node1', 'node5', 6)
        tree.addChild('node3', 'node6', 7)

        it('preorder traversal should return node in the right order', function () {
            const result = []
            for (const node of tree.preOrderTraversal('root')) {
                result.push(node)
            }
            expect(result).to.eql([1, 2, 5, 6, 3, 4, 7])
        })

        it('postorder traversal should return node in the right order', function () {
            const result = []
            for (const node of tree.postOrderTraversal('root')) {
                result.push(node)
            }
            expect(result).to.eql([5, 6, 2, 3, 7, 4, 1])
        })
    })

    describe('invalid operations', function () {
        const tree = new Tree(3, 'root')
        tree.addChild('root', 'node1', 4)
        tree.addChild('root', 'node2', 5)

        it('should throw error when query invalid node', function () {
            expect(tree.getNodeData.bind(tree, 'node3')).to.throws(InvalidTreeOperationError)
            expect(tree.getChildrenId.bind(tree, 'node3')).to.throws(InvalidTreeOperationError)
        })

        it('should throw error when add child to an invalid node', function () {
            expect(tree.addChild.bind(tree, 'node3', 'node4', 12)).to.throws(
                InvalidTreeOperationError
            )
        })

        it('should throw error when ask an invalid know about its child', function () {
            expect(tree.isChild.bind(tree, 'node3', 'node1')).to.throws(InvalidTreeOperationError)
        })
    })
})

import { Graph, InvalidGraphOperationError } from '../../../src/data-structures/graph'
import { expect } from 'chai'

describe('Tests for graphs', function () {
    describe('inserting and deleting edges', function () {
        const graph = new Graph<number>()
        graph.addVertex('v1', 1)
        graph.addVertex('v2', 2)
        graph.addVertex('v3', 3)
        graph.addEdge('v1', 'v2')
        graph.addEdge('v1', 'v3')
        graph.addEdge('v3', 'v2')

        it('should return the correct vertex data when given the corresponding id', function () {
            expect(graph.getVertex('v1')).to.equals(1)
            expect(graph.getVertex('v2')).to.equals(2)
            expect(graph.getVertex('v3')).to.equals(3)
        })

        it('edges should be connected correctly', function () {
            expect(graph.isConnected('v1', 'v2')).to.equals(true)
            expect(graph.isConnected('v1', 'v3')).to.equals(true)
            expect(graph.isConnected('v3', 'v2')).to.equals(true)
            expect(graph.isConnected('v2', 'v3')).to.equals(false)
            expect(graph.isConnected('v2', 'v1')).to.equals(false)
        })

        it('vertices should no longer be connected when their edge is deleted', function () {
            graph.deleteEdge('v3', 'v2')
            graph.deleteEdge('v1', 'v3')
            expect(graph.isConnected('v3', 'v2')).to.equals(false)
            expect(graph.isConnected('v1', 'v3')).to.equals(false)
        })
    })

    describe('deleting vertices', function () {
        const graph = new Graph<number>()
        graph.addVertex('v1', 1)
        graph.addVertex('v2', 2)
        graph.addVertex('v3', 3)
        graph.addVertex('v4', 4)
        graph.addVertex('v5', 5)
        graph.addEdge('v1', 'v2')
        graph.addEdge('v2', 'v3')
        graph.addEdge('v2', 'v4')
        graph.addEdge('v4', 'v5')
        graph.addEdge('v5', 'v4')
        graph.addEdge('v5', 'v2')

        it('both vertex and their edges to the other vertices are deleted', function () {
            expect(graph.isConnected('v5', 'v2')).to.equals(true)
            graph.deleteVertex('v2')
            expect(graph.getVertex.bind(graph, 'v2')).to.throws(InvalidGraphOperationError)
            expect(graph.isConnected('v5', 'v2')).to.equals(false)
        })
    })

    describe('traversal', function () {
        const graph = new Graph<number>()
        graph.addVertex('v1', 1)
        graph.addVertex('v2', 2)
        graph.addVertex('v3', 3)
        graph.addVertex('v4', 4)
        graph.addVertex('v5', 5)
        graph.addEdge('v1', 'v2')
        graph.addEdge('v2', 'v3')
        graph.addEdge('v2', 'v4')
        graph.addEdge('v4', 'v5')
        graph.addEdge('v5', 'v4')
        graph.addEdge('v5', 'v2')

        it('dfs should return the vertices in the correct order', function () {
            const result = []
            for (const vertexData of graph.dfs('v1')) {
                result.push(vertexData)
            }
            expect(result).to.eqls([1, 2, 3, 4, 5])
        })
    })

    describe('invalid operations', function () {
        const graph = new Graph<number>()
        graph.addVertex('v1', 1)
        graph.addVertex('v2', 2)
        graph.addVertex('v3', 3)
        graph.addEdge('v1', 'v2')
        graph.addEdge('v1', 'v3')
        graph.addEdge('v3', 'v2')

        it('query invalid vertex should throws an error', function () {
            expect(graph.getVertex.bind(graph, 'v4')).to.throws(InvalidGraphOperationError)
            expect(graph.getVertex.bind(graph, 'v5')).to.throws(InvalidGraphOperationError)
        })

        it('connect two edges where one of it do not exist should throw an error', function () {
            expect(graph.addEdge.bind(graph, 'v4', 'v5')).to.throws(InvalidGraphOperationError)
            expect(graph.addEdge.bind(graph, 'v4', 'v1')).to.throws(InvalidGraphOperationError)
            expect(graph.addEdge.bind(graph, 'v1', 'v4')).to.throws(InvalidGraphOperationError)
        })

        it('checking if an edge exists between a source vertex that does not exists should throws an error', function () {
            expect(graph.isConnected.bind(graph, 'v4', 'v1')).to.throws(InvalidGraphOperationError)
        })
    })
})

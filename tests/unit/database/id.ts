import Id, { InvalidIdError } from '../../../src/database/identifiers'
import { expect } from 'chai'

describe('Tests for Id', function () {
    describe('constructor', function () {
        it("id should contains two parts seperated by '_'", function () {
            const id = new Id().toString()
            const splitId = id.split('_')
            expect(splitId.length).to.equals(2)
        })

        it('id instiated after another id should have a larger timestamp value', function () {
            const id1 = new Id()
            setTimeout(() => {
                const id2 = new Id()
                expect(id1.compareTo(id2)).to.lessThan(0)
            }, 1000)
        })
    })

    describe('parser', function () {
        it('should parse id correctly', function () {
            const id = new Id()
            const id2 = new Id(id.toString())
            expect(id.compareTo(id2)).to.equals(0)
        })

        it('should throw error for invalid id', function () {
            const invalidId1 = '123_456'
            const invalidId2 = 'hello_world'
            const invalidId3 = 'notvalid*12'
            expect(() => new Id(invalidId1)).to.throws(InvalidIdError)
            expect(() => new Id(invalidId2)).to.throws(InvalidIdError)
            expect(() => new Id(invalidId3)).to.throws(InvalidIdError)
        })
    })
})

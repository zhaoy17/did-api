/* eslint-disable @typescript-eslint/no-explicit-any */

import * as chai from 'chai'
import { expect } from 'chai'
import chaiAsPromise from 'chai-as-promised'
import { createStubInstance } from 'sinon'
import { Pool } from 'pg'
import { PostgresDriver } from '../../../src/database/drivers/PotgresDriver'
import { DriverError } from '../../../src/database/drivers/Driver'
import { map, MapperError } from '../../../src/mapper'

chai.use(chaiAsPromise)

class MockModel {
    @map('example')
    data: string

    constructor(data: string) {
        this.data = data
    }
}

describe('tests for postgres driver', function () {
    describe('test queries', function () {
        it('driver.exeucte() should throw exception if driver throws an error', function () {
            const mockPool = createStubInstance(Pool)
            mockPool.query.rejects(new Error())

            const driver = new PostgresDriver(undefined, mockPool as any)
            return expect(driver.execute('', [])).eventually.rejectedWith(DriverError)
        })

        it('driver.execute() should return valid query output when execute', function () {
            const mockPool = createStubInstance(Pool)
            mockPool.query.resolves({ rows: [{ a: 'example' }] })

            const driver = new PostgresDriver(undefined, mockPool as any)
            return expect(driver.execute('', [])).eventually.deep.equals([{ a: 'example' }])
        })

        it('driver.query() should throw exception if driver throws an error', function () {
            const mockPool = createStubInstance(Pool)
            mockPool.query.rejects(new Error())

            const driver = new PostgresDriver(undefined, mockPool as any)
            return expect(driver.query('', [], MockModel)).eventually.rejectedWith(DriverError)
        })

        it('driver.query() should return a valid result', async function () {
            const mockPool = createStubInstance(Pool)
            mockPool.query.resolves({
                rows: [{ example: 'a' }, { example: 'b' }],
            })

            const driver = new PostgresDriver(undefined, mockPool as any)
            const mockModels = await driver.query('', [], MockModel)
            expect(mockModels.length).equals(2)
            expect(mockModels[0].data).equals('a')
            expect(mockModels[1].data).equals('b')
        })

        it('driver.query() should throws mapper error if we get an invalid query', async function () {
            const mockPool = createStubInstance(Pool)
            mockPool.query.resolves({
                rows: [{ exmple: 'a' }, { example: 'b' }],
            })

            const driver = new PostgresDriver(undefined, mockPool as any)
            return expect(driver.query('', [], MockModel)).eventually.rejectedWith(MapperError)
        })
    })
})

# DID API Docs
This API Doc is generated automatically from an [OpenAPI Specification](https://swagger.io/specification/) document through [widdershins](https://github.com/Mermade/widdershins) and [slate](https://github.com/slatedocs/slate). It is hosted by Github Page, and can be viewed [here](https://zhaoy17.github.io/did-api/).
